using Genie.Router

route("/") do
  serve_static_file("welcome.html")
end

route("/hello") do
  "Welcome to Genie"
end

route("/MyFirstWebpage") do
  serve_static_file("htmlclass.html")
end

route("/exercise4") do
  serve_static_file("exercise4.html")
end
